//Sebelum menjalankan progr|am ini cek dulu dimana USB sink wsn terdeteksi, 
//menggunakan perintah: dmesg | grep 'ttyUSB*' 
//misalnya jika terdeteksi di ttyUSB0, ubah permissions suatu user agar dapat membaca ttyUSB0, 
//menggunakan perintah: sudo chmod 777 /dev/ttyUSB0
//periksa apakah pc linux sudah bisa membaca Comm Port,
//misalnya menggunakan putty (putty harus terinstall pada linux) 
//compile program berikut dan jalankan
//setelah berhasil, kembangkan program berikut sesuai dengan arahan pada kuliah 10 Mei 2018 Mendatang.

#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>        
#include <stdlib.h> 
#include <sys/socket.h> /* for socket(), connect(), send(), and recv() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <unistd.h>     /* for close() */

#define RCVBUFSIZE 32
#define BAUDRATE B9600 
#define MODEMDEVICE "/dev/ttyUSB0"/*UART NAME IN PROCESSOR*/
//#define MODEMDEVICE "/dev/ttyACM0"/*UART NAME IN PROCESSOR*/
#define _POSIX_SOURCE 1 /* POSIX compliant source */
#define FALSE 0
#define TRUE 1
void openport(void);
void readport(void);
void DieWithError(char *errorMessage);  
int fd=0, n;
struct termios oldtp, newtp;
 
void  readport(void)
{
	int  ibuff;
	char buff[10];

  	n = read(fd, &buff, 5);
  		sscanf(buff,"%d",&ibuff);
  		printf("%d\n", ibuff); 	

	sprintf(buff, "%d ", ibuff);
	int sock;                        /* Socket descriptor */
    	struct sockaddr_in echoServAddr; /* Echo server address */
    	unsigned short echoServPort;     /* Echo server port */
    	char *servIP;                    /* Server IP address (dotted quad) */
    	char *echoString;                /* String to send to echo server */
    	char echoBuffer[RCVBUFSIZE];     /* Buffer for echo string */
    	unsigned int echoStringLen;      /* Length of string to echo */
    	int bytesRcvd, totalBytesRcvd;   /* Bytes read in single recv() 
                                        and total bytes read */

    
    	servIP = "127.0.0.1";             /* First arg: server IP address (dotted quad) */
    	//echoString = "q"; //5 string aja  
         /* Second arg: string to echo */

    	echoServPort = 1234;//coba2
    	/* Create a reliable, stream socket using TCP */
    	if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        	DieWithError("socket() failed");

    	/* Construct the server address structure */
    	memset(&echoServAddr, 0, sizeof(echoServAddr));     /* Zero out structure */
    	echoServAddr.sin_family      = AF_INET;             /* Internet address family */
    	echoServAddr.sin_addr.s_addr = inet_addr(servIP);   /* Server IP address */
    	echoServAddr.sin_port        = htons(echoServPort); /* Server port */

    	/* Establish the connection to the echo server */
    	if (connect(sock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) < 0)
        	DieWithError("connect() failed");

    	echoStringLen = strlen(buff);          /* Determine input length */

    	/* Send the string to the server */
    	if (send(sock, buff, echoStringLen, 0) != echoStringLen)
        	DieWithError("send() sent a different number of bytes than expected");

    	close(sock);
    	exit(0);
	
}

void openport(void)
{
         
	 fd = open(MODEMDEVICE, O_RDWR | O_NOCTTY |O_NDELAY );
         if (fd <0)
         {
         	perror(MODEMDEVICE);
 
         }
                                                                                
         fcntl(fd,F_SETFL,0);
         tcgetattr(fd,&oldtp); /* save current serial port settings */
         bzero(&newtp, sizeof(newtp));
         newtp.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
	 newtp.c_iflag = IGNPAR | ICRNL;                                                                       
         newtp.c_oflag = 0;                                                                        
         newtp.c_lflag = ICANON;                                                                    
         newtp.c_cc[VINTR]    = 0;     /* Ctrl-c */
         newtp.c_cc[VQUIT]    = 0;     /* Ctrl-\ */
         newtp.c_cc[VERASE]   = 0;     /* del */
         newtp.c_cc[VKILL]    = 0;     /* @ */
        // newtp.c_cc[VEOF]     = 4;     /* Ctrl-d */
         newtp.c_cc[VTIME]    = 0;     /* inter-character timer unused */
         newtp.c_cc[VMIN]     = 0;     /* blocking read until 1 character arrives */
         newtp.c_cc[VSWTC]    = 0;     /* '\0' */
         newtp.c_cc[VSTART]   = 0;     /* Ctrl-q */
         newtp.c_cc[VSTOP]    = 0;     /* Ctrl-s */
         newtp.c_cc[VSUSP]    = 0;     /* Ctrl-z */
         newtp.c_cc[VEOL]     = 0;     /* '\0' */
         newtp.c_cc[VREPRINT] = 0;     /* Ctrl-r */
         newtp.c_cc[VDISCARD] = 0;     /* Ctrl-u */
         newtp.c_cc[VWERASE]  = 0;     /* Ctrl-w */
         newtp.c_cc[VLNEXT]   = 0;     /* Ctrl-v */
         newtp.c_cc[VEOL2]    = 0;     /* '\0' */

	 
}

void DieWithError(char *errorMessage)
{
    perror(errorMessage);
    exit(1);
}
 
int main()
{
	openport();
	readport();
        return 0;
    
}

