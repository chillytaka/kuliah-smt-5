#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

int main(void)
{
    int fd;
    int nr;
    char buf[20];

    if ((fd=open("myread.txt", O_RDONLY)) < 0) {
        perror("Error Open File");
      	exit (1);
    }

    nr = read(fd,buf,6);
    printf("%d\n",nr);
    printf("%s\n",buf);

    nr = read(fd,buf,6);
    printf("%d\n",nr);
    printf("%s\n",buf);

    nr = read(fd,buf,6);
    printf("%d\n",nr);
    printf("%s\n",buf);

    close(fd);
}
