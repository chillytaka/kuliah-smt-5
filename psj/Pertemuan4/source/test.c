#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), bind(), and connect() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_ntoa() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */

FILE *file;

void test()
{
    fprintf(file, "NYOBAK THOK");
    printf("INI DAIR SANA \n");
}

int main(int argc, char *argv[])
{
    file = fopen("test.txt", "w");
    if (file == NULL)
    {
        perror("ERROR");
        exit(1);
    }

    fprintf(file, "TEST SADJA \n");
    printf("TSET \n");

    test();
}