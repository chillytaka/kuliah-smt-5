#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), connect(), send(), and recv() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <termios.h>
#include <fcntl.h>

#define RCVBUFSIZE 32   /* Size of receive buffer */

void DieWithError(char *errorMessage);  /* Error handling function */

int main(int argc, char *argv[])
{
    int sock;                        /* Socket descriptor */
    struct sockaddr_in echoServAddr; /* Echo server address */
    unsigned short echoServPort;     /* Echo server port */
    char *servIP;                    /* Server IP address (dotted quad) */
    char *echoString;                /* String to send to echo server */
    char echoBuffer[RCVBUFSIZE];     /* Buffer for echo string */
    unsigned int echoStringLen;      /* Length of string to echo */
    int bytesRcvd, totalBytesRcvd;   /* Bytes read in single recv()
                                        and total bytes read */
    int fd, sleepnya, sleep_awal, sleeptak;
    char *idnya;

    if ((argc < 2) || (argc > 3))    /* Test for correct number of arguments */
    {
       fprintf(stderr, "Usage: %s <Server IP> [<Echo Port>]\n",
               argv[0]);
       exit(1);
    }

    servIP = argv[1];             /* First arg: server IP address (dotted quad) */

    if (argc == 3)
        echoServPort = atoi(argv[2]); /* Use given port, if any */
    else
        echoServPort = 7;  /* 7 is the well-known port for the echo service */

    /* Create a reliable, stream socket using TCP */
    if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        DieWithError("socket() failed");

    /* Construct the server address structure */
    memset(&echoServAddr, 0, sizeof(echoServAddr));     /* Zero out structure */
    echoServAddr.sin_family      = AF_INET;             /* Internet address family */
    echoServAddr.sin_addr.s_addr = inet_addr(servIP);   /* Server IP address */
    echoServAddr.sin_port        = htons(echoServPort); /* Server port */

    /* Establish the connection to the echo server */
    if (connect(sock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) < 0)
        DieWithError("connect() failed");

    if(fork()==0)
    {
        idnya="3";
        sleep_awal=0;
        sleeptak=1;
    }
    else
    {
        idnya="4";
        sleep_awal=3000000;
        sleeptak=1;
    }

     while(1)
     {
         /* read up to 128 bytes from the fd */
         if(sleeptak==1)
         {
             usleep(sleep_awal);
             sleeptak=0;
         }
         int r = (rand() % (40 + 1 - 20)) + 20;
         char rstring[2];
         sprintf(rstring, "%d", r);
         int newSize = strlen(rstring)  + strlen(idnya) + 1;
         char * newBuffer = (char *)malloc(newSize);
         strcpy(newBuffer,idnya);
         strcat(newBuffer,rstring);
         echoStringLen = strlen(newBuffer);          /* Determine input length */

        /* Send the string to the server */
        if(echoStringLen>0)
        {
            if (send(sock, newBuffer, echoStringLen, 0) != echoStringLen)
            DieWithError("send() sent a different number of bytes than expected");

            printf("%s\n", newBuffer);    /* Print a final linefeed */
        }
        usleep(1000*1000);
     }


    close(sock);
    exit(0);
}

void DieWithError(char *errorMessage)
{
    perror(errorMessage);
    exit(1);
}

