#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), connect(), send(), and recv() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <termios.h>
#include <fcntl.h>

#define RCVBUFSIZE 32   /* Size of receive buffer */

void DieWithError(char *errorMessage);  /* Error handling function */

int main(int argc, char *argv[])
{
    int sock;                        /* Socket descriptor */
    struct sockaddr_in echoServAddr; /* Echo server address */
    unsigned short echoServPort;     /* Echo server port */
    char *servIP;                    /* Server IP address (dotted quad) */
    char *echoString;                /* String to send to echo server */
    char echoBuffer[RCVBUFSIZE];     /* Buffer for echo string */
    unsigned int echoStringLen;      /* Length of string to echo */
    int bytesRcvd, totalBytesRcvd;   /* Bytes read in single recv()
                                        and total bytes read */
    int fd,sleeptak,sleep_awal;
    char *portnya, *idnya;

    if ((argc < 2) || (argc > 3))    /* Test for correct number of arguments */
    {
       fprintf(stderr, "Usage: %s <Server IP> [<Echo Port>]\n",
               argv[0]);
       exit(1);
    }

    servIP = argv[1];             /* First arg: server IP address (dotted quad) */

    if (argc == 3)
        echoServPort = atoi(argv[2]); /* Use given port, if any */
    else
        echoServPort = 7;  /* 7 is the well-known port for the echo service */

    /* Create a reliable, stream socket using TCP */
    if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        DieWithError("socket() failed");

    /* Construct the server address structure */
    memset(&echoServAddr, 0, sizeof(echoServAddr));     /* Zero out structure */
    echoServAddr.sin_family      = AF_INET;             /* Internet address family */
    echoServAddr.sin_addr.s_addr = inet_addr(servIP);   /* Server IP address */
    echoServAddr.sin_port        = htons(echoServPort); /* Server port */

    /* Establish the connection to the echo server */
    if (connect(sock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) < 0)
        DieWithError("connect() failed");

    if(fork()==0)
    {
        portnya = "/dev/ttyUSB0";
        idnya = "1";
        sleep_awal=0;
        sleeptak=1;
    }
    else
    {
        portnya = "/dev/ttyACM0";
        idnya = "2";
        sleep_awal=3000000;
        sleeptak=1;
    }

    fd = open(portnya, O_RDWR | O_NOCTTY);

    /* Set up the control structure */
     struct termios toptions;

     /* Get currently set options for the tty */
     tcgetattr(fd, &toptions);

    /* Set custom options */

    /* 115200 baud */
     cfsetispeed(&toptions, B9600);
     cfsetospeed(&toptions, B9600);

        /* 8 bits, no parity, no stop bits */
     toptions.c_cflag &= ~PARENB;
     toptions.c_cflag &= ~CSTOPB;
     toptions.c_cflag &= ~CSIZE;
     toptions.c_cflag |= CS8;
     /* Canonical mode */
     toptions.c_lflag |= ICANON;
    /* commit the options */
     tcsetattr(fd, TCSANOW, &toptions);

    /* Wait for the Arduino to reset */
     usleep(1000*1000);
     /* Flush anything already in the serial buffer */
     tcflush(fd, TCIFLUSH);
     while(1)
     {
         if(sleeptak==1)
         {
             usleep(sleep_awal);
             sleeptak=0;
         }
         /* read up to 128 bytes from the fd */
        int n = read(fd, echoString, 128);
         //echoString[n] = 0;
        int newSize = strlen(idnya)  + n + 1;
        char * newBuffer = (char *)malloc(newSize);
        strcpy(newBuffer,idnya);
        strcat(newBuffer,echoString);

        echoStringLen = strlen(newBuffer);          /* Determine input length */

        /* Send the string to the server */
        if(echoStringLen>0)
        {
            if (send(sock, newBuffer, echoStringLen, 0) != echoStringLen)
            DieWithError("send() sent a different number of bytes than expected");

            printf("%s\n", newBuffer);    /* Print a final linefeed */
        }
        usleep(1000*1000);
     }


    close(sock);
    exit(0);
}

void DieWithError(char *errorMessage)
{
    perror(errorMessage);
    exit(1);
}

