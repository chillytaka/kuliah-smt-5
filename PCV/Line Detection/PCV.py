import cv2
import numpy as np
 
def apply_brightness_contrast(input_img, brightness = 0, contrast = 0):

    if brightness != 0:
        if brightness > 0:
            shadow = brightness
            highlight = 255
        else:
            shadow = 0
            highlight = 255 + brightness
        alpha_b = (highlight - shadow)/255
        gamma_b = shadow

        buf = cv2.addWeighted(input_img, alpha_b, input_img, 0, gamma_b)
    else:
        buf = input_img.copy()

    if contrast != 0:
        f = 131*(contrast + 127)/(127*(131-contrast))
        alpha_c = f
        gamma_c = 127*(1-f)

        buf = cv2.addWeighted(buf, alpha_c, buf, 0, gamma_c)

    return buf


# Create a VideoCapture object and read from input file
# If the input is the camera, pass 0 instead of the video file name
cap = cv2.VideoCapture('PCVSD.mp4')
 
# Check if camera opened successfully
if (cap.isOpened()== False): 
  print("Error membuka video stream or file")
 
# Read until video is completed
while(cap.isOpened()):
  # Capture frame-by-frame
  ret, frame = cap.read()
  if ret == True:
     
    # Crop image
    imCrop = frame[int(0):int(600), int(0):int(1366)]
 

    ter = apply_brightness_contrast(imCrop, 5, 75)

    
    # Display cropped image
    cv2.imshow("Image", ter)

    # Convert BGR to HSV
    hsv = cv2.cvtColor(ter, cv2.COLOR_BGR2HSV)

    min_range = np.array([50, 20, 20])
    max_range = np.array([60, 255, 255])

    chroma = cv2.inRange(hsv, min_range, max_range)
    inverse_chroma = ~chroma
    chroma_result = cv2.bitwise_and(imCrop, imCrop, mask=inverse_chroma)
    
    edges = cv2.Canny(chroma_result,100,200)

    # Display the resulting imCrop
    
    cv2.imshow('imCrop1',edges)
    #cv2.imshow('imCrop2',imCrop)
    #cv2.imshow('imCrop3',result)
 
    # Press Q on keyboard to  exit
    if cv2.waitKey(10) & 0xFF == ord('q'):
      break
 
  # Break the loop
  else: 
    break
 
# When everything done, release the video capture object
cap.release()
 
# Closes all the frames
cv2.destroyAllWindows()

