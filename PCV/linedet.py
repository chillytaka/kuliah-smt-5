import cv2
import numpy as np


cap = cv2.VideoCapture('drive.mp4')

while (cap.isOpened()):
    ret, frame = cap.read()

    img_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # lower range and max range (green)
    min_range = np.array([150, 150, 150])
    max_range = np.array([255, 255, 255])

    chroma = cv2.inRange(img_hsv, min_range, max_range)
    inverse_chroma = ~chroma
    chroma_result = cv2.bitwise_and(frame, frame, mask=inverse_chroma)

    cv2.imshow("win1", frame)
    cv2.imshow("win2", img_hsv)
    cv2.imshow("win3", chroma)
    cv2.imshow("win4", chroma_result)

    if cv2.waitKey(25) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
