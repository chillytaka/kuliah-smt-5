import cv2
import numpy as np


cap = cv2.VideoCapture('video.mp4')
img2 = cv2.imread('img2.jpg')

while (1):
    ret, frame = cap.read()

    img_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # lower range and max range (green)
    min_range = np.array([50, 20, 20])
    max_range = np.array([60, 255, 255])

    chroma = cv2.inRange(img_hsv, min_range, max_range)
    inverse_chroma = ~chroma
    chroma_result = cv2.bitwise_and(frame, frame, mask=inverse_chroma)
    img2_result = cv2.bitwise_and(img2, img2, mask=chroma)

    result = cv2.add(chroma_result, img2_result)

    cv2.imshow("win1", frame)
    cv2.imshow("win2", img_hsv)
    cv2.imshow("win4", chroma)
    cv2.imshow("win5", result)

    if cv2.waitKey(25) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
